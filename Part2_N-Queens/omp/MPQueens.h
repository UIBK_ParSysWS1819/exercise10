#ifndef EXERCISE04_MPQUEENS_H
#define EXERCISE04_MPQUEENS_H

#include "Queens.h"
#include <iostream>

class MPQueens: public Queens {

public:
    explicit MPQueens(unsigned long problem_size)
            : Queens(problem_size) {}

    explicit MPQueens() : Queens() {}

    /***
     * Check if queen could be placed safely without intersecting other queens
     * @param x row position of the new possible queen
     * @param y column position of the new possible queen
     * @param solution previously placed queens
     */
    bool is_safe(int x, int y,  const std::vector <int> &solution) const {
        // Check if queen could be placed safely without intersecting other queens
        for( auto row=0; row< x;row++ ){
            if (solution[row] == y ||  // same column
                solution[row] + row == y + x ||  // same diagonal
                solution[row] - row == y - x){   // same diagonal
                return false;
            }
        }
        return true;
    }

    /***
     * Collect all possible solutions for the new chess field extended by one row.
     * The new queen is placed based on the new row for each one of the possible new solutions
     * (if it is not intersecting with the previous solutions).
     */
    void addQueen(int row, int columnn, std::vector <int> &queens){
        // Test if there are any conflicts with this possible solution
        if (is_safe(row, columnn, queens)){
            queens[row] = columnn;
            // visited last row
            if(row == problem_size-1){
#pragma omp critical
                solutions.push_back(queens);
            } else{
                for (int i= 0; i < problem_size; i++){
                    addQueen(row +1 , i,queens);
                }
            }
        }
    }

    /***
     * Adapted implementation of python code from <A HREF="https://de.wikipedia.org/wiki/Damenproblem#Anzahl_der_L%C3%B6sungen_im_klassischen_Damenproblem"> n-queens german wikipedia</a>
     * @param row
     * @param column
     * @return a list of vectors(chessboards) with the queens saved as queens[row] = column of the current queen
     */
    void solve_nqueens(int row, int column){

#pragma omp parallel
        {
#pragma omp single
            // all possible start configurations
            for (auto i = 0; i < column; i++) {
                std::vector<int> queens;
                queens.resize(problem_size, -1);

#pragma omp task firstprivate(queens)
                addQueen(0, i, queens);
            }
        }
    }

    std::vector<std::vector <int>> solve() override {
        solutions = std::vector<std::vector <int>>{};
        if(problem_size == 0){
            solutions.push_back(std::vector <int>{});
        } else {
            solve_nqueens(problem_size,problem_size);
        }
        return solutions;
    }
};


#endif //EXERCISE04_MPQUEENS_H
