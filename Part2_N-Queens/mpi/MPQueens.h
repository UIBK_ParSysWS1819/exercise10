#ifndef EXERCISE04_MPQUEENS_H
#define EXERCISE04_MPQUEENS_H

#include "Queens.h"
#include <iostream>

class MPQueens: public Queens {

public:
    explicit MPQueens(unsigned long problem_size)
    : Queens(problem_size) {}

    explicit MPQueens() : Queens() {}

    /***
     * Check if queen could be placed safely without intersecting other queens
     * @param x row position of the new possible queen
     * @param y column position of the new possible queen
     * @param solution previously placed queens
     */
    bool is_safe(int x, int y,  const std::vector <int> &solution) const {
        // Check if queen could be placed safely without intersecting other queens
        for( auto row=0; row< x;row++ ){
            if (solution[row] == y ||  // same column
                solution[row] + row == y + x ||  // same diagonal
                solution[row] - row == y - x){   // same diagonal
                return false;
            }
        }
        return true;
    }

    /***
     * Collect all possible solutions for the new chess field extended by one row.
     * The new queen is placed based on the new row for each one of the possible new solutions
     * (if it is not intersecting with the previous solutions).
     */
    void addQueen(int row, int columnn, std::vector <int> &queens, std::vector <std::vector <int>> &solutions){
        // Test if there are any conflicts with this possible solution
        if (is_safe(row, columnn, queens)){
            queens[row] = columnn;
            // visited last row
            if(row == problem_size-1){
                solutions.push_back(queens);
            } else{
                for (int i= 0; i < problem_size; i++){
                    addQueen(row +1 , i,queens, solutions);
                }
            }
        }
    }

    /***
     * Adapted implementation of python code from <A HREF="https://de.wikipedia.org/wiki/Damenproblem#Anzahl_der_L%C3%B6sungen_im_klassischen_Damenproblem"> n-queens german wikipedia</a>
     * @param row
     * @param column
     * @return a list of vectors(chessboards) with the queens saved as queens[row] = column of the current queen
     */
    void solve_nqueens(int row, int column, int pid, int num_processes){
        int amount_pid_solutions = 0;
        int sum_pid_solutions = 0;

            // all possible start configurations
            for (auto i = 0; i < column; i++) {
                int target_pid = i % num_processes;
                std::vector<int> queens(problem_size, -1);
                // total solutions
                std::vector<std::vector <int>> solutions;

                // create problem chunks based on start position for each process
                if(target_pid == pid){
                    addQueen(0, i, queens, solutions);
                    amount_pid_solutions += solutions.size();
                    //std::cout << "PID " << pid << " with startpos " << i <<" has " << solutions.size() <<" solutions" << std::endl;
                }
            }
            // Collect solutions from processes
            MPI::COMM_WORLD.Reduce(&amount_pid_solutions, &sum_pid_solutions, 1, MPI::UNSIGNED_LONG, MPI::SUM, 0);
            
            // omitted individual solutions and just counted the amount
    }

    std::vector<std::vector <int>> solve(int pid, int num_processes) override {
        solutions = std::vector<std::vector <int>>{};
        if(problem_size == 0){
            solutions.push_back(std::vector <int>{});
        } else {
            solve_nqueens(problem_size,problem_size, pid, num_processes);
        }
        return solutions;
    }

};


#endif //EXERCISE04_MPQUEENS_H
