#ifndef EXERCISE10_QUEENS_H
#define EXERCISE10_QUEENS_H

#include <vector>
#include <list>
#include <mpi.h>


class Queens {
protected:
    //position of the queen in each row
    std::vector <int> queens;
    // total solutions
    std::vector<std::vector <int>> solutions;
public:
    unsigned long problem_size;
    Queens() {
        problem_size = 8;
        // eight queens standard
        queens.resize(problem_size,-1);
    }

    explicit Queens(unsigned long problem_size) : problem_size(problem_size) {
        queens.resize(problem_size,-1);
    }

    virtual std::vector<std::vector <int>> solve(int pid, int num_processes) = 0;

    void print_board(std::vector<int> queens);
    void print_solutions();
    unsigned long get_amount_solutions(){
        return solutions.size();
    }

};


#endif //EXERCISE10_QUEENS_H
