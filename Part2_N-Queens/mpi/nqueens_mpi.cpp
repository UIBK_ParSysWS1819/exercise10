#include <iostream>
#include <chrono>
#include "MPQueens.h"
#include "../../chrono_timer.h"


/*
 * Calculating the number of solutions to the N-Queens problem. Calculating the number of solutions to the N-Queens
 * problem. The input parameter for the program is the number N, and its output is the number of possible solutions.
 */



int main(int argc, char** argv) {
    MPI::Init(argc, argv);  // Initialize MPI

    int pid_master = 0;
    int num_processes = MPI::COMM_WORLD.Get_size();     // Get the total number of processes available.
    int pid = MPI::COMM_WORLD.Get_rank();               // Get the individual process ID.

//    int N = 15; //default
    int N = 8;
    if(argc == 2) {
        N = std::stoi(argv[1]);
    }

    /*if (pid == pid_master)
        std::cout << "N: " << N << std::endl;*/

    MPQueens queens = MPQueens(N);

    MPI::COMM_WORLD.Barrier(); // for correct time measurement

    ChronoTimer timer("MPI NQueens");

    queens.solve(pid, num_processes);

    //long time = timer.getElapsedTime_msecs();
    double time = timer.getElapsedTime_secs();

    MPI::COMM_WORLD.Barrier();

    if (pid == pid_master) {
        //std::cout << "Time: " << time << std::endl;
        std::cout << time << std::endl;

        //std::cout << "Amount solutions: " << queens.get_amount_solutions() << std::endl;
    }
    MPI::COMM_WORLD.Barrier();
    MPI::Finalize();    // Terminate MPI.
    return EXIT_SUCCESS;
}