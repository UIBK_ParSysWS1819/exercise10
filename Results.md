## 10.1 - Distributed Memory Dense Matrix-Matrix Multiplication
### Questions
**Consider the most advantageous data distribution pattern, and how to efficiently gather the results:**

We are using contiguous memory for the 2D Matrix, which makes it easy to send blocks of data around.

For gathering the results why tried a few different approaches:
- Using send and receive, where each process send their data to the master process: Here we have one version (`mmul`), which first
sends the start position and size of the data block so that the master knows the amount of data received. The other version  (`mmul_1`)
only has the send-recv for the data and the master calculates the start and end position and size of the data to receive from each slave.
- Using allReduce to reduce the different matrices from each process into one matrix. (not measured!)
- And finally using gather, where the master processes gathers the data from each slave and appends them automatically, which seems to
be suited the best (`mmul_2`)

**How do these considerations change when you want to perform a sequence of multiple multiplications in which you re-use your output matrix?**

You may be able to reuse the local result for the next multiplication before synchronizing the whole matrix.

If we had a sequence of matrices, we could parallelize the multiplication of pairs of matrices
before joining them together in some kind of reduction.
(E.g. ((A * B) * C) * D = ((A * B) * (C * D))

### Benchmarking Results
mmul:

size|nslots|median_val_mpi \[s\]
---:|---:|---:
3072|1|92.032600
3072|2|69.407200
3072|4|41.190600
3072|8|38.275700
3072|16|19.319900
3072|32|9.851940
3072|64|5.172910

mmul_1:

size|nslots|median_val_mpi \[s\]
---:|---:|---:
3072|1|92.557900
3072|2|70.448800
3072|4|41.484800
3072|8|38.265000
3072|16|19.330800
3072|32|9.956240
3072|64|5.182120


mmul_2:

size|nslots|median_val_mpi \[s\]
---:|---:|---:
3072|1|81.794800
3072|2|67.237100
3072|4|40.795200
3072|8|38.381900
3072|16|19.580000
3072|32|10.435100
3072|64|6.454750


## 10.2 Distributed Memory N-Queens Solver
### Questions
+ How would an optimal parallelization pattern of this this program look like for our small cluster? 
+ And how would it differ if you had a supercomputer with more than 10000 nodes available?

An optimal parallelization pattern would have to distribute the work equal among the different nodes, so that no node remains idle.
The problem is the equal distribution among nodes. Some positions lead to larger set of possible solutions than others.
Those nodes that dispose most of their search paths would need new search paths to start from.
    
    Divide the search for the solution into smaller junks.
    Split the problem into smaller subtrees and distribute those among the nodes. 
    The inactive nodes distribute and synchronize until the a certain depth is reached.
    Starting from those solutions continue with the next set of rows.

### Benchmarking Results
MPI:

size|nslots|median_val_mpi \[s\]
---:|---:|---:
15|1|61.826400
15|2|32.533300
15|4|16.348400
15|8|8.424860
15|16|4.629810
15|32|4.631080
15|64|4.657030

OpenMP:

|Size|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|
|15|62.750200 s|33.889400 s|17.642000 s|9.895750 s|

As you can see MPI is a second faster than OpenMP on a single node, using 1, 2, 4, and 8 processes / threads.

## 10.3 Benchmarking Stencil, again
### Benchmarking Results
size|eps|nslots|median_val_mpi \[s\]
---:|---:|---:|---:
512|1|1|56.375400
512|1|2|28.562600
512|1|4|14.791600
512|1|8|7.663500
512|1|16|4.937170
512|1|32|4.497430
512|1|64|13.257200
768|1|1|256.441000
768|1|2|138.367000
768|1|4|65.577900
768|1|8|32.422800
768|1|16|18.645300
768|1|32|14.025800
768|1|64|27.180400