#!/bin/bash


echo 'Compiling gcc'

module load gcc/8.2.0
module load openmpi/3.1.1

mpic++ -std=c++11 -O3 -march=native ../../Part3_Stencil/main2D_3.cpp -o ../../Part3_Stencil/stencil2D_3_mpi

module unload openmpi/3.1.1
module unload gcc/8.2.0

#####################################################################################################################

#echo 'Compiling icc'

#module load intel/15.0
#module load openmpi/3.1.1  # intel/15.0 seems not to work with openmpi/3.1.1
#module load openmpi/2.1.1

#mpic++ -std=c++11 -O3 -march=native ../../Part3_Stencil/main2D.cpp -o ../../Part3_Stencil/stencil2D_3_mpi_icc

#module unload openmpi/3.1.1    # intel/15.0 seems not to work with openmpi/3.1.1
#module unload openmpi/2.1.1
#module unload intel/15.0
