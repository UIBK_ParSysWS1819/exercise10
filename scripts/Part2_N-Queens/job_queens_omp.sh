#!/bin/bash

#$ -N queens_test

#$ -q std.q

#$ -cwd

#$ -o output_queens.out

#$ -e output_queens.err

#$ -pe openmp 8

./bench_queens_omp.sh
