#!/bin/bash


sizes=(15)
files=("nqueens_mpi")
for index in ${!sizes[*]}
do
	for file in ${files[*]}
	do
		for nslots in 1 2 4 8
		do
			echo "submitting job with size: ${size[$index]} having openmpi-"${nslots}"perhost $nslots"
			qsub -N ${file} -q std.q -cwd -o ./${file}/times/${file}_${sizes[$index]}_${nslots}.out -e ./${file}/errors/${file}_${sizes[$index]}_${nslots}.err -l excl=true -l h_vmem="4G" -pe openmpi-${nslots}perhost $nslots measure.sh ${sizes[$index]} ${nslots} ${file}
		done

		for nslots in 16 32 64
		do
			echo "submitting job with size: ${size[$index]} having openmpi-8perhost $nslots"
			qsub -N ${file} -q std.q -cwd -o ./${file}/times/${file}_${sizes[$index]}_${nslots}.out -e ./${file}/errors/${file}_${sizes[$index]}_${nslots}.err -l excl=true -l h_vmem="4G" -pe openmpi-8perhost $nslots measure.sh ${sizes[$index]} ${nslots} ${file}
		done
	done
done
