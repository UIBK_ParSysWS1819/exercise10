#!/bin/bash

#module load gcc/8.2.0

median () {
    # sort times
    #times=($(printf '%ld\n' "${@}" | sort -n))  # for long values (msec)
    export LC_NUMERIC=en_US.utf-8   # for double values (sec)
    times=($(printf '%lf\n' "${@}" | sort -g))
    size=${#times[@]}

    local res_val   # define local variable within function

    if (( $size % 2 == 0 )); then
        val1=${times[$size / 2 - 1]}
        val2=${times[$size / 2]}

        #res_val=$(( ($val1 + $val2) / 2 )) # for long values (msec)
        res_val=$(echo $val1 $val2 | awk '{printf "%lf", ($1 + $2) / 2}')    # for double values (sec)
    else
        res_val=${times[ $size / 2 ]}
    fi

    echo "$res_val"     # return median value
}


# N-Queens problem
printf "|Size|1T|2T|4T|8T|\n"
printf "|---:|---:|---:|---:|---:|\n"


runs=5

for size in 15
do
    printf "|%s|" $size

    for threads in 1 2 4 8
    do
        times=()

        for (( run = 1; run <= $runs; run++ ))
        do
            export OMP_NUM_THREADS=$threads
            out_par=`../../Part2_N-Queens/omp/nqueens_omp $size`

            times+=($out_par)
        done

        median_val_omp=$(median ${times[@]})    # get return value of median

        #printf "%s ms|" $median_val_omp    # for long values (msec)
        printf "%s s|" $median_val_omp  # for double values (sec)
    done

    printf "\n"
done

#module unload gcc/8.2.0