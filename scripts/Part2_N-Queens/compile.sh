#!/bin/bash


echo 'Compiling gcc'

module load gcc/8.2.0

g++ -std=c++11 -O3 -fopenmp ../../Part2_N-Queens/omp/nqueens_omp.cpp -o ../../Part2_N-Queens/omp/nqueens_omp

module load openmpi/3.1.1

mpic++ -std=c++11 -O3 -march=native ../../Part2_N-Queens/mpi/nqueens_mpi.cpp -o ../../Part2_N-Queens/mpi/nqueens_mpi

module unload openmpi/3.1.1
module unload gcc/8.2.0

#####################################################################################################################

#echo 'Compiling icc'

#module load intel/15.0

#g++ -std=c++11 -O3 ../../Part2_N-Queens/omp/nqueens_omp.cpp -o ../../Part2_N-Queens/omp/nqueens_omp

#module load openmpi/3.1.1  # intel/15.0 seems not to work with openmpi/3.1.1
#module load openmpi/2.1.1

#mpic++ -std=c++11 -O3 -march=native ../../Part2_N-Queens/mpi/nqueens_mpi.cpp -o ../../Part2_N-Queens/mpi/nqueens_mpi_icc

#module unload openmpi/3.1.1    # intel/15.0 seems not to work with openmpi/3.1.1
#module unload openmpi/2.1.1
#module unload intel/15.0
