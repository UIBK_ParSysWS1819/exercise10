#!/bin/bash

for file in "nqueens_mpi"
do
	mkdir $file
	cd $file
	mkdir errors
	mkdir times
	cd ..
done
