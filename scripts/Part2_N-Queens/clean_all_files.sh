#!/bin/bash

for file in "nqueens_mpi"
do
	echo "rm ${file}/errors/*"
	echo "rm ${file}/times/*"
	rm ${file}/errors/*
	rm ${file}/times/*
done
