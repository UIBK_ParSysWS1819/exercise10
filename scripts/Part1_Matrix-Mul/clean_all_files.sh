#!/bin/bash

for file in "mmul_mpi" "mmul_mpi_1" "mmul_mpi_2"
do
	echo "rm ${file}/errors/*"
	echo "rm ${file}/times/*"
	rm ${file}/errors/*
	rm ${file}/times/*
done
