#!/bin/bash

for file in "mmul_mpi" "mmul_mpi_1" "mmul_mpi_2"
do
cd $file
	directory=`echo "${PWD##*/}"`

	echo "size|nslots|median_val_mpi \\[s\\]" > ${directory}_output.csv
	echo "---:|---:|---:" >> ${directory}_output.csv
	
	for filename in times/*.out; do
		#echo "$file $directory $filename"
		more $filename >> ${directory}_output.csv
	done

	for filename in errors/*.err; do
		#echo "$file $directory $filename"
		more $filename >> ${directory}_errors.txt
	done
cd ..
done
