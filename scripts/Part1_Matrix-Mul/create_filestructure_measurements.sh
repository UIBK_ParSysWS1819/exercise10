#!/bin/bash

for file in "mmul_mpi" "mmul_mpi_1" "mmul_mpi_2"
do
	mkdir $file
	cd $file
	mkdir errors
	mkdir times
	cd ..
done
