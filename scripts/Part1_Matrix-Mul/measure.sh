#!/bin/bash

module load gcc/8.2.0
module load openmpi/3.1.1

median () {
    # sort times
    #times=($(printf '%ld\n' "${@}" | sort -n))  # for long values (msec)
    export LC_NUMERIC=en_US.utf-8   # for double values (sec)
    times=($(printf '%lf\n' "${@}" | sort -g))
    size=${#times[@]}

    local res_val   # define local variable within function

    if (( $size % 2 == 0 )); then
        val1=${times[$size / 2 - 1]}
        val2=${times[$size / 2]}

        res_val=$(( ($val1 + $val2) / 2 ))
    else
        res_val=${times[ $size / 2 ]}
    fi

    echo "$res_val"     # return median value
}

runs=5
size=$1
nslots=$2
filename=$3

times=()

for (( run = 1; run <= $runs; run++ ))
do
	out_par=`mpirun -np $nslots ../../Part1_Matrix-Mul/mpi/$filename $size `

	times+=($out_par)
done

median_val_mpi=$(median ${times[@]})    # get return value of median

printf "%s|%s|%s\n" $size $nslots $median_val_mpi

module unload openmpi/3.1.1
module unload gcc/8.2.0
