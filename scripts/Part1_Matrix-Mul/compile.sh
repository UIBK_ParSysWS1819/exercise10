#!/bin/bash


echo 'Compiling gcc'

module load gcc/8.2.0
module load openmpi/3.1.1

mpic++ -std=c++11 -O3 -march=native ../../Part1_Matrix-Mul/mpi/mmul_mpi.cpp -o ../../Part1_Matrix-Mul/mpi/mmul_mpi
mpic++ -std=c++11 -O3 -march=native ../../Part1_Matrix-Mul/mpi/mmul_mpi_1.cpp -o ../../Part1_Matrix-Mul/mpi/mmul_mpi_1
mpic++ -std=c++11 -O3 -march=native ../../Part1_Matrix-Mul/mpi/mmul_mpi_2.cpp -o ../../Part1_Matrix-Mul/mpi/mmul_mpi_2

module unload openmpi/3.1.1
module unload gcc/8.2.0

#####################################################################################################################

#echo 'Compiling icc'

#module load intel/15.0
#module load openmpi/3.1.1  # intel/15.0 seems not to work with openmpi/3.1.1
#module load openmpi/2.1.1

#mpic++ -std=c++11 -O3 -march=native ../../Part1_Matrix-Mul/mpi/mmul_mpi.cpp -o ../../Part1_Matrix-Mul/mpi/mmul_mpi_icc
#mpic++ -std=c++11 -O3 -march=native ../../Part1_Matrix-Mul/mpi/mmul_mpi_1.cpp -o ../../Part1_Matrix-Mul/mpi/mmul_mpi_1_icc
#mpic++ -std=c++11 -O3 -march=native ../../Part1_Matrix-Mul/mpi/mmul_mpi_2.cpp -o ../../Part1_Matrix-Mul/mpi/mmul_mpi_2_icc

#module unload openmpi/3.1.1    # intel/15.0 seems not to work with openmpi/3.1.1
#module unload openmpi/2.1.1
#module unload intel/15.0
