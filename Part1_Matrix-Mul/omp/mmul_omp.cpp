#include <vector>
#include <cstdlib>
#include <omp.h>
#include "../../chrono_timer.h"


struct Matrix : std::vector<std::vector<double>> {
	unsigned matSize;

	Matrix() : matSize(0) {}

    explicit Matrix(unsigned n) : matSize(n) {
		resize(n);

		for(unsigned i = 0; i < n; ++i) {
			(*this)[i].resize(n);
		}
	}

	double& operator()(unsigned i, unsigned j) {
		return (*this)[i][j];
	}

	const double& operator()(unsigned i, unsigned j) const {
		return (*this)[i][j];
	}
};

// initializes a matrix of size n x n
template<typename F>
Matrix init(unsigned n, F initFun) {
    Matrix res(n);

    for(unsigned i = 0; i < n; i++) {
        for(unsigned j = 0; j < n; j++) {
            res(i,j) = initFun(i, j);
        }
    }

    return res;
}

Matrix operator*(const Matrix& a, const Matrix& b) {
    unsigned n = a.matSize;

    Matrix c = init(n, [](unsigned, unsigned) {
                        return 0;
                    });

    // switched loops, usually i, j, k
    #pragma omp parallel for
    for(unsigned i = 0; i < n; ++i) {
        for(unsigned k = 0; k < n; ++k) {
            for(unsigned j = 0; j < n; ++j) {
                c(i, j) += a(i, k) * b(k, j);
            }
        }
    }

    return c;
}


int main(int argc, char** argv) {
    if(argc != 2) {
        std::cout << "Amount of program arguments is not 2!" << std::endl;
        return EXIT_FAILURE;
    }

    unsigned n = atoi(argv[1]);

    if(n == 0) {
        std::cout << "Matrix size is 0!" << std::endl;
        return EXIT_FAILURE;
    }

    // create two matrices
    auto a = init(n, [](unsigned i, unsigned j) {
                    return (i == 0) && (j == 0) ? 42.0 : 0.0;
                });

    auto b = init(n, [](unsigned i, unsigned j) {
                    return (j >= i) ? 1.0 : 0.0;
                });

    auto expected_ret = init(n, [](unsigned i, unsigned j) {
                    return (i == 0) ? 42.0 : 0.0;
                });

    Matrix c;

    ChronoTimer t("OpenMP Matrix Mul");

    // compute the product
    c = a * b;

    //long time = t.getElapsedTime_msecs();
    double time = t.getElapsedTime_secs();

    std::cout << "Time: " << time << std::endl;
    //std::cout << time << std::endl;

    // check that the result is correct
    auto ret = (c == expected_ret) ? EXIT_SUCCESS : EXIT_FAILURE;

    return ret;
}
