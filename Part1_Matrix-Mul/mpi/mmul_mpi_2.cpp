#include <vector>
#include <cstdlib>
#include <mpi.h>
#include "../../chrono_timer.h"

unsigned pid_master = 0;

unsigned get_start(unsigned N, int num_processes, int pid){
    return pid * N / num_processes;
}

unsigned get_end(unsigned N, int num_processes, int pid){
    unsigned step_size = N / num_processes;
    unsigned start_row = pid * step_size;
    return (pid == num_processes - 1 && N % num_processes != 0) ? N : start_row + step_size;
}


struct Matrix : std::vector<double> {
    unsigned matSize;
    int num_processes;
    int pid;

    Matrix() : matSize(0) {}

    explicit Matrix(unsigned n, int num_processes, int pid) : matSize(n), num_processes(num_processes), pid(pid) {
        resize(n * n);
    }

    double& operator()(unsigned i, unsigned j) {
        return (*this)[i * matSize + j];
    }

    const double& operator()(unsigned i, unsigned j) const {
        return (*this)[i * matSize + j];
    }

    void print() {
        for(unsigned i = 0; i < matSize; ++i) {
            for(unsigned j = 0; j < matSize; ++j) {
                std::cout << (*this)(i, j) << " ";
            }
            std::cout << "\n";
        }
        std::cout << std::endl;
    }

    Matrix operator* (Matrix a) {
        Matrix c(matSize, num_processes, pid);

        unsigned start_row = get_start(matSize, num_processes, pid);
        unsigned end_row = get_end(matSize, num_processes, pid);
        unsigned block_size = (end_row - start_row) * matSize;

        // Calculate
        for(unsigned i = start_row; i < end_row; ++i) {
            for(unsigned k = 0; k < matSize; ++k) {
                for(unsigned j = 0; j < matSize; ++j) {
                    c(i, j) += (*this)(i, k) * a(k, j);
                }
            }
        }

        // sync
        if (num_processes > 1) {
            MPI::COMM_WORLD.Gather(&c(start_row, 0), block_size, MPI::DOUBLE, &c(0, 0), block_size, MPI::DOUBLE, pid_master);
        }

        return c;
    }
};

// initializes a matrix of size n x n
template<typename F>
Matrix init(unsigned n, int num_proc, int pid, F initFun) {
    Matrix res(n, num_proc, pid);

    for(unsigned i = 0; i < n; i++) {
        for(unsigned j = 0; j < n; j++) {
            res(i, j) = initFun(i, j);
        }
    }

    return res;
}

int main(int argc, char** argv) {
    MPI::Init(argc, argv);  // Initialize MPI

    int num_processes = MPI::COMM_WORLD.Get_size();     // Get the total number of processes available.
    int pid = MPI::COMM_WORLD.Get_rank();               // Get the individual process ID.

    if (argc != 2) {
        std::cout << "Amount of program arguments is not 2!" << std::endl;
        MPI::Finalize();    // Terminate MPI.
        return EXIT_FAILURE;
    }

    unsigned N = atoi(argv[1]);

    if (N == 0) {
        std::cout << "Matrix size is 0!" << std::endl;
        MPI::Finalize();    // Terminate MPI.
        return EXIT_FAILURE;
    }

    if (num_processes > N) {
        std::cout << "Matrix is smaller than available processes!" << std::endl;
        MPI::Finalize();    // Terminate MPI.
        return EXIT_FAILURE;
    }

    // create two matrices
    auto a = init(N, num_processes, pid, [](unsigned i, unsigned j) {
        return (i == 0) && (j == 0) ? 42.0 : 0.0;
    });

    auto b = init(N, num_processes, pid, [](unsigned i, unsigned j) {
        return (j >= i) ? 1.0 : 0.0;
    });

    auto expected_ret = init(N, num_processes, pid, [](unsigned i, unsigned j) {
        return (i == 0) ? 42.0 : 0.0;
    });

    MPI::COMM_WORLD.Barrier(); // for correct time measurement

    ChronoTimer timer("MPI Matrix Mul");

    Matrix c = a * b;

    //long time = timer.getElapsedTime_msecs();
    double time = timer.getElapsedTime_secs();

    MPI::COMM_WORLD.Barrier();

    int ret = 0;

    if (pid == pid_master) {
//        expected_ret.print();
//        std::cout << std::endl;
//        c.print();
//        std::cout << std::endl;
        //std::cout << "Time: " << time << std::endl;
        std::cout << time << std::endl;

        // check that the result is correct
        ret = (c == expected_ret) ? EXIT_SUCCESS : EXIT_FAILURE;
    }

    MPI::Finalize();    // Terminate MPI.

    return ret;
}