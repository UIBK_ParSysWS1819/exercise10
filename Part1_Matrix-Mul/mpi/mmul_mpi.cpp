#include <vector>
#include <cstdlib>
#include <mpi.h>
#include "../../chrono_timer.h"


struct Matrix : std::vector<double> {
    unsigned matSize;

    Matrix() : matSize(0) {}

    explicit Matrix(unsigned n) : matSize(n) {
        resize(n * n);
    }

    double& operator()(unsigned i, unsigned j) {
        return (*this)[i * matSize + j];
    }

    const double& operator()(unsigned i, unsigned j) const {
        return (*this)[i * matSize + j];
    }
};

// initializes a matrix of size n x n
template<typename F>
Matrix init(unsigned n, F initFun) {
    Matrix res(n);

    for(unsigned i = 0; i < n; i++) {
        for(unsigned j = 0; j < n; j++) {
            res(i, j) = initFun(i, j);
        }
    }

    return res;
}

void print_matrix(const Matrix &mat, const std::string &description) {
    unsigned N = mat.matSize;

    std::cout << description << ":\n";

    for(unsigned i = 0; i < N; ++i) {
        for(unsigned j = 0; j < N; ++j) {
            std::cout << mat(i, j) << " ";
        }

        std::cout << "\n";
    }

    std::cout << std::endl;
}

void synchronize_results(int pid, int pid_master, int num_processes, unsigned start_row, int block_size, Matrix &c) {
    int tag_start_row = 0;
    int tag_block_size = 1;
    int tag_data = 2;

    if(pid == pid_master) { // master receives computed parts
        unsigned start_row_slave;
        unsigned block_size_slave;

        // collect results from each slaves
        for(int current_slave_pid = 1; current_slave_pid < num_processes; current_slave_pid++) {
            // first get boundaries of slaves in order know where to place the slave's data in master
            MPI::COMM_WORLD.Recv(&start_row_slave, 1, MPI::INT, current_slave_pid, tag_start_row);
            MPI::COMM_WORLD.Recv(&block_size_slave, 1, MPI::INT, current_slave_pid, tag_block_size);

            MPI::COMM_WORLD.Recv(&c(start_row_slave, 0), block_size_slave, MPI::DOUBLE, current_slave_pid, tag_data);
        }
    }
    else {  // slaves send computed parts to master
        MPI::COMM_WORLD.Send(&start_row, 1, MPI::INT, pid_master, tag_start_row);
        MPI::COMM_WORLD.Send(&block_size, 1, MPI::INT, pid_master, tag_block_size);

        MPI::COMM_WORLD.Send(&c(start_row, 0), block_size, MPI::DOUBLE, pid_master, tag_data);
    }
}

void multiply(unsigned N, unsigned start_row, unsigned end_row, const Matrix& a, const Matrix& b, Matrix& c) {
    //c = a * b;
    // switched loops, usually i, j, k
    for(unsigned i = start_row; i < end_row; ++i) {
        for(unsigned k = 0; k < N; ++k) {
            for(unsigned j = 0; j < N; ++j) {
                c(i, j) += a(i, k) * b(k, j);
            }
        }
    }
}

void matrix_multiplication(unsigned N, int num_processes, int pid, int pid_master, const Matrix& a, const Matrix& b, Matrix& c) {
    unsigned step_size = N / num_processes;
    unsigned start_row = pid * step_size;
    unsigned end_row;

    // if the matrix block_size is not equally divisible by the number of processes
    // then the last process has to do the remaining rows additionally
    if(pid == num_processes - 1 && N % num_processes != 0) {
        end_row = N;
    }
    else {
        end_row = start_row + step_size;
    }

    unsigned block_size = (end_row - start_row) * N;

    //std::cout << "PID: " << pid << " step_size: " << step_size << " start_row: " << start_row << " end_row: " << end_row - 1 << " block_size: " << block_size << std::endl;

    // compute the product
    c = init(N, [](unsigned, unsigned) {
        return 0;
    });

    multiply(N, start_row, end_row, a, b, c);

    // let slaves synchronize with master
    if(num_processes > 1) {
        synchronize_results(pid, pid_master, num_processes, start_row, block_size, c);
    }
}

int main(int argc, char** argv) {
    MPI::Init(argc, argv);  // Initialize MPI

    int pid_master = 0;
    int num_processes = MPI::COMM_WORLD.Get_size();     // Get the total number of processes available.
    int pid = MPI::COMM_WORLD.Get_rank();               // Get the individual process ID.

    if (argc != 2) {
        std::cout << "Amount of program arguments is not 2!" << std::endl;
        MPI::Finalize();    // Terminate MPI.
        return EXIT_FAILURE;
    }

    unsigned N = atoi(argv[1]);

    if (N == 0) {
        std::cout << "Matrix size is 0!" << std::endl;
        MPI::Finalize();    // Terminate MPI.
        return EXIT_FAILURE;
    }

    if (num_processes > N) {
        std::cout << "Matrix is smaller than available processes!" << std::endl;
        MPI::Finalize();    // Terminate MPI.
        return EXIT_FAILURE;
    }

    // create two matrices
    auto a = init(N, [](unsigned i, unsigned j) {
        return (i == 0) && (j == 0) ? 42.0 : 0.0;
    });

    auto b = init(N, [](unsigned i, unsigned j) {
        return (j >= i) ? 1.0 : 0.0;
    });

    auto expected_ret = init(N, [](unsigned i, unsigned j) {
        return (i == 0) ? 42.0 : 0.0;
    });

    Matrix c;

    MPI::COMM_WORLD.Barrier(); // for correct time measurement

    ChronoTimer timer("MPI Matrix Mul");

    matrix_multiplication(N, num_processes, pid, pid_master, a, b, c);

    //long time = timer.getElapsedTime_msecs();
    double time = timer.getElapsedTime_secs();

    MPI::COMM_WORLD.Barrier();

    int ret = 0;

    if (pid == pid_master) {
        //print_matrix(expected_ret, "\nexpected");
        //print_matrix(c, "c");
        //std::cout << "Time: " << time << std::endl;
        std::cout << time << std::endl;

        // check that the result is correct
        ret = (c == expected_ret) ? EXIT_SUCCESS : EXIT_FAILURE;
    }

    MPI::Finalize();    // Terminate MPI.

    return ret;
}